<%--
  Created by IntelliJ IDEA.
  User: tranv
  Date: 11/11/2019
  Time: 3:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Phones list</title>
</head>
<body><h1>Phones</h1>
<p>
    <a href="/phones?action=create">Create new phone</a>
</p>
<table border="1">
    <tr>
        <td>Name</td>
        <td>Amount</td>
        <td>Information</td>
        <td>Image</td>
        <td>BrandName</td>
        <td>Create By</td>
        <td>Create Date</td>
        <td>Modify By</td>
        <td>Modify Date</td>
        <td>Delete By</td>
        <td>Delete Date</td>
        <td>Edit</td>
        <td>Delete</td>
    </tr>
    <c:forEach items='${requestScope["phonesList"]}' var="phone">
        <tr>
            <td><a href="/phones?action=view&id=${phone.getIdPhone()}">${phone.getName()}</a></td>
            <td>${phone.getAmount()}</td>
            <td>${phone.getInformation()}</td>
            <td>${phone.getImage()}</td>
            <td>${phone.getBrandName()}</td>
            <td>${phone.getCreateBy()}</td>
            <td>${phone.getCreateDate()}</td>
            <td>${phone.getModifyBy()}</td>
            <td>${phone.getModifyDate()}</td>
            <td>${phone.getDeleteBy()}</td>
            <td>${phone.getDeleteDate()}</td>

            <td><a href="/phones?action=edit&id=${phone.getIdPhone()}">edit</a></td>
            <td><a href="/phones?action=delete&id=${phone.getIdPhone()}">delete</a></td>
        </tr>
    </c:forEach>
</table></body>
</html>
