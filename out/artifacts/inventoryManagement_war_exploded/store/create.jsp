<%--
  Created by IntelliJ IDEA.
  User: tranv
  Date: 11/11/2019
  Time: 3:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create Store</title>
    <style>
        .message {
            color:green;
        }
    </style>
</head>
<body>
<h1>Create Store</h1>
<p> <c:if test='${requestScope["message"] != null}'>
    <span class="message">${requestScope["message"]}</span>
</c:if>
</p>
<p>
    <a href="/store">Back to Store</a>
</p>
<form method="post">
    <fieldset>
        <legend>Store information</legend>
        <table>
            <tr>
                <td> Name Store:</td>
                <td><input type="text" name="nameStore" id = "nameStore"> </td>
                <td> Address Store:</td>
                <td><input type="text" name="addressStore" id = "addressStore"> </td>
                <td> PhoneNumber:</td>
                <td><input type="text" name="PhoneNumberStore" id = "PhoneNumberStore"> </td>
            </tr>
            <tr>
                <td>create By:</td>
                <td><input type="text" name="createBy" id = "createBy"/> </td>
            </tr>
            <tr>
                <td><input type="submit" value="Create Store" ></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>
