<%--
  Created by IntelliJ IDEA.
  User: tranv
  Date: 11/11/2019
  Time: 3:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Deleting customer</title>
</head>
<body>
<h1>Delete store</h1>
<p>
    <a href="/store">Back to storelist</a>
</p>
<form method="post">
    <h3>Are you sure?</h3>
    <fieldset>
        <legend>store information</legend>
        <table>
            <tr>
                <td>Name Store: </td>
                <td>${requestScope["stores"].getNameStore()}</td>
            </tr>
            <tr>
                <td>Address: </td>
                <td>${requestScope["stores"].getAddressStore()}</td>
            </tr>
            <tr>
                <td>PhoneNumber: </td>
                <td>${requestScope["stores"].getPhoneNumberStore()}</td>
            </tr>
            <tr>
                <td>Delete By: </td>
                <td> <input type="text" name = "deleteBy" id = "deleteBy"/> </td>
            </tr>

            <tr>
                <td><input type="submit" value="Delete store"></td>
                <td><a href="/store">Back to store list</a></td>
            </tr>
        </table>
    </fieldset>
</body>
</html>
