<%--
  Created by IntelliJ IDEA.
  User: tranv
  Date: 11/11/2019
  Time: 3:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Store</title>
</head>
<body>
<h1>Edit Store</h1>
<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>
<p>
    <a href="/store">Back to Store list</a>
</p>
<form method="post">
    <fieldset>
        <legend>Store information</legend>
        <table>
            <tr>
                <td>Name Store: </td>
                <td><input type="text" name="nameStore" id="nameStore" value="${requestScope["stores"].getNameStore()}"></td>
            </tr>
            <tr>
                <td>Address Store: </td>
                <td><input type="text" name="addressStore" id="addressStore" value="${requestScope["stores"].getAddressStore()}"></td>
            </tr>
            <tr>
                <td>PhoneNumber Store </td>
                <td><input type="text" name="PhoneNumberStore" id="PhoneNumberStore" value="${requestScope["stores"].getPhoneNumberStore()}"></td>
            </tr>
            <tr>
                <td>Modify By</td>
                <td> <input type="text" name = "modifyBy" id = "modifyBy"/> </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Update Store"></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>
