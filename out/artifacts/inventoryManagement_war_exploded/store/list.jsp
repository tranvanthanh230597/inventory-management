<%--
  Created by IntelliJ IDEA.
  User: tranv
  Date: 11/11/2019
  Time: 3:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Store list</title>
</head>
<body>
<h1>Store list</h1>
<p><a href="/store?action=create">Create new store</a> </p>
<table border="1">
    <tr>
        <td>id Store</td>
        <td>Name</td>
        <td>Address</td>
        <td>Phone Number</td>
        <td>Delete By</td>
        <td>Delete Date</td>
        <td>Modify By</td>
        <td>Modify Date</td>
        <td>Create By</td>
        <td>Create Date</td>
        <td>Edit</td>
        <td>Delete</td>

    </tr>
    <c:forEach items='${requestScope["stores"]}' var="store">
        <tr>
            <td>${store.getIdStore()}</td>
            <td><a href="/store?action=view&idStore=${store.getIdStore()}">${store.getNameStore()}</a></td>
            <td>${store.getAddressStore()}</td>
            <td>${store.getPhoneNumberStore()}</td>
            <td>${store.getDeleteBy()}</td>
            <td>${store.getDeleteDate()}</td>
            <td>${store.getModifyBy()}</td>
            <td>${store.getModifyDate()}</td>
            <td>${store.getCreatBy()}</td>
            <td>${store.getCreatDate()}</td>
            <td><a href="/store?action=edit&idStore=${store.getIdStore()}">edit</a></td>
            <td><a href="/store?action=delete&idStore=${store.getIdStore()}">delete</a></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
