package service;

import model.DetailStock;

import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DetailStockJDBCServiceImpl implements DetailStockService {
    private String jdbcURL = "jdbc:mysql://localhost:3306/inventory_management";
    private String jdbcUsername = "codegym";
    private String jdbcPassword = "codegym.123";

    private static final String INSERT_DETAIL_STOCKS_SQL = "INSERT INTO Detail Stock"
            + "(idStock, idPhone, phoneAmount, phoneNumber," +
            " deleteBy, deleteDate, modifyBy, modifyDate, createBy, createDate) VALUES"
            + "(?,?,?,?,?,?,?,?,?,?);";

    //Lưu ý phần này
    private static final String SELECT_DETAIL_STOCKS_BY_ID_STOCK = "select idStock,name,address, phoneNumber," +
            "deleteBy, deleteDate, modifyBy, modifyDate, createBy, createDate from Stock where idStock =?";
    private static final String SELECT_ALL_DETAIL_STOCKS = "select * from DetailStock";
    private static final String DELETE_DETAIL_STOCKS_SQL = "delete from DetailStock where idStock = ?;";
    private static final String UPDATE_DETAIL_STOCKS_SQL = "update DetailStock set name=?, address=?, phoneNumber=?," +
            "deleteBy=?, deleteDate=?, modifyBy=?, modifyDate=?, createBy=?, createDate=? where idStock =?";

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return connection;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

    @Override
    public List<DetailStock> findAll() {
        // using try-with-resources to avoid closing resources (boiler plate code)
        List<DetailStock> detailstocks = new ArrayList<>();
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();
             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_DETAIL_STOCKS);) {
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                int idStock = rs.getInt("ID Stock");
                int idPhone = rs.getInt("ID Phone");
                int phoneAmount = rs.getInt("Phone Amount");
                String deleteBy = rs.getString("Delete By");
                String deleteDate = rs.getString("Delete Date");
                String modifyBy = rs.getString("Modify By");
                String modifyDate = rs.getString("Modify Date");
                String createBy = rs.getString("Create By");
                String createDate = rs.getString("Create Date");
                detailstocks.add(new DetailStock(idStock, idPhone, phoneAmount,
                        deleteBy, deleteDate, modifyBy, modifyDate, createBy, createDate));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return detailstocks;
    }

    //Lưu ý phần này
    @Override
    public void save(DetailStock detailstock) {
        System.out.println(INSERT_DETAIL_STOCKS_SQL);
        // try-with-resource statement will auto close the connection.
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_DETAIL_STOCKS_SQL)) {
            preparedStatement.setInt(1, detailstock.getIdPhone());
            preparedStatement.setInt(2, detailstock.getPhoneAmount());
            preparedStatement.setString(3, detailstock.getDeleteBy());
            preparedStatement.setString(4, detailstock.getDeleteDate());
            preparedStatement.setString(5, detailstock.getModifyBy());
            preparedStatement.setString(6, detailstock.getModifyDate());
            preparedStatement.setString(7, detailstock.getCreatBy());
            preparedStatement.setString(8, detailstock.getCreatDate());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public DetailStock findById(int idStock) {
        DetailStock detailstock = null;
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();
             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_DETAIL_STOCKS_BY_ID_STOCK);) {
            preparedStatement.setInt(1, idStock);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                int idPhone = rs.getInt("ID Phone");
                int phoneAmount = rs.getInt("Phone Amount");
                String deleteBy = rs.getString("Delete By");
                String deleteDate = rs.getString("Delete Date");
                String modifyBy = rs.getString("Modify By");
                String modifyDate = rs.getString("Modify Date");
                String createBy = rs.getString("Create By");
                String createDate = rs.getString("Create Date");
                detailstock = new DetailStock(idStock, idPhone, phoneAmount,
                        deleteBy, deleteDate, modifyBy, modifyDate, createBy, createDate);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return detailstock;
    }

    //Lưu ý phần này
    @Override
    public void update(int idStock, DetailStock detailstock) {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_DETAIL_STOCKS_SQL);) {
            statement.setInt(1, detailstock.getIdStock());
            statement.setInt(2, detailstock.getIdPhone());
            statement.setInt(3, detailstock.getPhoneAmount());
            statement.setString(4, detailstock.getDeleteBy());
            statement.setString(5, detailstock.getDeleteDate());
            statement.setString(6, detailstock.getModifyBy());
            statement.setString(7, detailstock.getModifyDate());
            statement.setString(8, detailstock.getCreatBy());
            statement.setString(9, detailstock.getCreatDate());
            rowUpdated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            printSQLException(e);
        }
        //return rowUpdated;
    }

    @Override
    public void remove(int idStock) {
        {
            try (Connection connection = getConnection();
                 PreparedStatement statement = connection.prepareStatement(DELETE_DETAIL_STOCKS_SQL);) {
                statement.setInt(1, idStock);
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}