package service;

import model.Staff;

import java.util.List;

public class StaffJDBCServiceImpl implements StaffService {
    @Override
    public List<Staff> showAll() {
        return null;
    }

    @Override
    public void save(Staff staff) {

    }

    @Override
    public Staff findById(int id) {
        return null;
    }

    @Override
    public void update(int id, Staff staff) {

    }

    @Override
    public void remove(int id) {

    }
}
