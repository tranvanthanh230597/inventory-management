package service;

import model.Stock;

import java.util.List;

public interface StockService {
    List<Stock> findAll();

    void save(Stock stock);

    Stock findById(int idStock);

    void update(int idStock, Stock stock);

    void remove(int idStock);
}
