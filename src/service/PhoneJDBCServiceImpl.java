package service;

import model.Phone;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PhoneJDBCServiceImpl implements PhoneService {

    private String jdbcURL = "jdbc:mysql://localhost:3306/inventory_management";
    private String jdbcUsername = "codegym";
    private String jdbcPassword = "codegym.123";

    private static final String SELECT_ALL_PHONES = "SELECT *FROM phone inner join brand on brand.idBrand = phone.idBrand where phone.isDelete = 0 and brand.isDelete = 0;";
    private static final String SELECT_PHONE_BY_IDBRAND = "SELECT *FROM phone inner join brand on brand.idBrand = phone.idBrand where phone.isDelete = 0 and brand.isDelete = 0 and phone.idBrand = ?;";
    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        }catch (SQLException | ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return connection;
    }
    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }


    @Override
    public List<Phone> findAll() {
        List<Phone>phones = new ArrayList<>();
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_PHONES);){
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                int idPhone =rs.getInt("idPhone");
                String phoneName = rs.getString("phone.name");
                int amount = rs.getInt("amount");
                String information = rs.getString("information");
                String image = rs.getString("image");
                String brandName = rs.getString("brand.name");
                String createBy = rs.getString("phone.createBy");
                String deleteBy = rs.getString("phone.deleteBy");
                String deleteDate = rs.getString("phone.deleteDate");
                String modifyBy = rs.getString("phone.modifyBy");
                String modifyDate = rs.getString("phone.modifyDate");
                String createDate = rs.getString("phone.createDate");
                phones.add(new Phone(idPhone,phoneName,amount,information,image,brandName,deleteBy,deleteDate,modifyBy,modifyDate,createBy,createDate));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return phones;


    }

    @Override
    public void save(Phone phone) {

    }

    @Override
    public Phone findById(int id) {
        return null;
    }

    @Override
    public void update(int id, Phone phone) {

    }

    @Override
    public void remove(int id) {

    }
    @Override
    public List<Phone> findAllByIdBrand(int id){
        List<Phone>phones = new ArrayList<>();
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PHONE_BY_IDBRAND);){
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                int idPhone =rs.getInt("idPhone");
                String phoneName = rs.getString("phone.name");
                int amount = rs.getInt("amount");
                String information = rs.getString("information");
                String image = rs.getString("image");
                String brandName = rs.getString("brand.name");
                String createBy = rs.getString("phone.createBy");
                String deleteBy = rs.getString("phone.deleteBy");
                String deleteDate = rs.getString("phone.deleteDate");
                String modifyBy = rs.getString("phone.modifyBy");
                String modifyDate = rs.getString("phone.modifyDate");
                String createDate = rs.getString("phone.createDate");
                phones.add(new Phone(idPhone,phoneName,amount,information,image,brandName,deleteBy,deleteDate,modifyBy,modifyDate,createBy,createDate));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return phones;
    }
}
