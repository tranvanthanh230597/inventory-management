package service;

import model.DetailStock;

import java.util.List;

public interface DetailStockService {
    List<DetailStock> findAll();

    void save(DetailStock detailStock);

    DetailStock findById(int idStock);

    void update(int Stock, DetailStock detailStock);

    void remove(int id);
}
