package controller;

import model.Stock;
import service.StockJDBCServiceImpl;
import service.StockService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "StockServlet", urlPatterns = "/stocks")
public class StockServlet extends HttpServlet {

    //private StockService stockService = new StockServiceImpl();
    private StockService stockService = new StockJDBCServiceImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action == null){
            action = "";
        }
        switch (action){
            case "create":
                createStock(request, response);
                break;
            case "edit":
                updateStock(request, response);
                break;
            case "delete":
                deleteStock(request, response);
                break;
            default:
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action == null){
            action = "";
        }
        switch (action){
            case "create":
                showCreateForm(request, response);
                break;
            case "edit":
                showEditForm(request, response);
                break;
            case "delete":
                showDeleteForm(request, response);
                break;
            case "view":
                viewStock(request, response);
                break;
            default:
                listStocks(request, response);
                break;
        }
    }

    private void viewStock(HttpServletRequest request, HttpServletResponse response) {
        int idStock = Integer.parseInt(request.getParameter("idStock"));
        Stock stock = this.stockService.findById(idStock);
        RequestDispatcher dispatcher;
        if(stock == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("stock", stock);
            dispatcher = request.getRequestDispatcher("stock/view.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteStock(HttpServletRequest request, HttpServletResponse response) {
        int idStock = Integer.parseInt(request.getParameter("idStock"));
        Stock stock = this.stockService.findById(idStock);
        RequestDispatcher dispatcher;
        if(stock == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            this.stockService.remove(idStock);
            try {
                response.sendRedirect("/stocks");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showDeleteForm(HttpServletRequest request, HttpServletResponse response) {
        int idStock = Integer.parseInt(request.getParameter("idStock"));
        Stock stock = this.stockService.findById(idStock);
        RequestDispatcher dispatcher;
        if(stock == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("stock", stock);
            dispatcher = request.getRequestDispatcher("stock/delete.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Lưu ý phần này
    private void updateStock(HttpServletRequest request, HttpServletResponse response) {
        int idStock = Integer.parseInt(request.getParameter("idStock"));
        String name = request.getParameter("Name");
        String address = request.getParameter("Address");
        String phoneNumber = request.getParameter("Phone Number");
        String deleteBy = request.getParameter("Delete By");
        String deleteDate = request.getParameter("Delete Date");
        String modifyBy = request.getParameter("Modify By");
        String modifyDate = request.getParameter("Modify Date");
        String createBy = request.getParameter("Create By");
        String createDate = request.getParameter("Create Date");
        Stock stock = this.stockService.findById(idStock);
        RequestDispatcher dispatcher;
        if(stock == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            stock.setName(name);
            stock.setAddress(address);
            stock.setPhoneNumber(phoneNumber);
            stock.setDeleteBy(deleteBy);
            stock.setDeleteDate(deleteDate);
            stock.setModifyBy(modifyBy);
            stock.setModifyDate(modifyDate);
            stock.setCreatBy(createBy);
            stock.setCreatDate(createDate);
            this.stockService.update(idStock, stock);
            request.setAttribute("stock", stock);
            request.setAttribute("message", "Stock information was updated");
            dispatcher = request.getRequestDispatcher("stock/edit.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response) {
        int idStock = Integer.parseInt(request.getParameter("idStock"));
        Stock stock = this.stockService.findById(idStock);
        RequestDispatcher dispatcher;
        if(stock == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("stock", stock);
            dispatcher = request.getRequestDispatcher("stock/edit.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createStock(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("Name");
        String address = request.getParameter("Address");
        String phoneNumber = request.getParameter("Phone Number");
        String deleteBy = request.getParameter("Delete By");
        String deleteDate = request.getParameter("Delete Date");
        String modifyBy = request.getParameter("Modify By");
        String modifyDate = request.getParameter("Modify Date");
        String createBy = request.getParameter("Create By");
        String createDate = request.getParameter("Create Date");
        int idStock = (int)(Math.random() * 10000);

        Stock stock = new Stock(idStock, name, address, phoneNumber,
                deleteBy, deleteDate, modifyBy, modifyDate, createBy, createDate);
        this.stockService.save(stock);
        RequestDispatcher dispatcher = request.getRequestDispatcher("stock/create.jsp");
        request.setAttribute("message", "New stock was created");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher = request.getRequestDispatcher("stock/create.jsp");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listStocks(HttpServletRequest request, HttpServletResponse response) {
        List<Stock> stocks = this.stockService.findAll();
        request.setAttribute("stocks", stocks);

        RequestDispatcher dispatcher = request.getRequestDispatcher("stock/list.jsp");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
