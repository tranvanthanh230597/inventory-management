package controller;

import model.DetailStock;
import service.DetailStockService;
import service.DetailStockJDBCServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "DetailStockServlet", urlPatterns = "/detailstocks")
public class DetailStockServlet extends HttpServlet {

    //private DetailStockService detailstockService = new DetailStockServiceImpl();
    private DetailStockService detailstockService = new DetailStockJDBCServiceImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action == null){
            action = "";
        }
        switch (action){
            case "create":
                createDetailStock(request, response);
                break;
            case "edit":
                updateDetailStock(request, response);
                break;
            case "delete":
                deleteDetailStock(request, response);
                break;
            default:
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action == null){
            action = "";
        }
        switch (action){
            case "create":
                showCreateForm(request, response);
                break;
            case "edit":
                showEditForm(request, response);
                break;
            case "delete":
                showDeleteForm(request, response);
                break;
            case "view":
                viewDetailStock(request, response);
                break;
            default:
                listDetailStocks(request, response);
                break;
        }
    }

    private void viewDetailStock(HttpServletRequest request, HttpServletResponse response) {
        int idStock = Integer.parseInt(request.getParameter("idStock"));
        DetailStock detailstock = this.detailstockService.findById(idStock);
        RequestDispatcher dispatcher;
        if(detailstock == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("detailstock", detailstock);
            dispatcher = request.getRequestDispatcher("detailstock/view.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteDetailStock(HttpServletRequest request, HttpServletResponse response) {
        int idStock = Integer.parseInt(request.getParameter("idStock"));
        DetailStock detailstock = this.detailstockService.findById(idStock);
        RequestDispatcher dispatcher;
        if(detailstock == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            this.detailstockService.remove(idStock);
            try {
                response.sendRedirect("/detailstocks");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showDeleteForm(HttpServletRequest request, HttpServletResponse response) {
        int idStock = Integer.parseInt(request.getParameter("idStock"));
        DetailStock detailstock = this.detailstockService.findById(idStock);
        RequestDispatcher dispatcher;
        if(detailstock == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("detailstock", detailstock);
            dispatcher = request.getRequestDispatcher("detailstock/delete.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Lưu ý phần này
    private void updateDetailStock(HttpServletRequest request, HttpServletResponse response) {
        int idStock = Integer.parseInt(request.getParameter("ID Stock"));
        int idPhone = Integer.parseInt(request.getParameter("ID Phone"));
        int phoneAmount = Integer.parseInt(request.getParameter("Phone Amount"));
        String deleteBy = request.getParameter("Delete By");
        String deleteDate = request.getParameter("Delete Date");
        String modifyBy = request.getParameter("Modify By");
        String modifyDate = request.getParameter("Modify Date");
        String createBy = request.getParameter("Create By");
        String createDate = request.getParameter("Create Date");
        DetailStock detailstock = this.detailstockService.findById(idStock);
        RequestDispatcher dispatcher;
        if(detailstock == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            detailstock.setIdPhone(idPhone);
            detailstock.setPhoneAmount(phoneAmount);
            detailstock.setDeleteBy(deleteBy);
            detailstock.setDeleteDate(deleteDate);
            detailstock.setModifyBy(modifyBy);
            detailstock.setModifyDate(modifyDate);
            detailstock.setCreatBy(createBy);
            detailstock.setCreatDate(createDate);
            this.detailstockService.update(idStock, detailstock);
            request.setAttribute("detailstock", detailstock);
            request.setAttribute("message", "Detail stock information was updated");
            dispatcher = request.getRequestDispatcher("detailstock/edit.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response) {
        int idStock = Integer.parseInt(request.getParameter("idStock"));
        DetailStock detailstock = this.detailstockService.findById(idStock);
        RequestDispatcher dispatcher;
        if(detailstock == null){
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("detailstock", detailstock);
            dispatcher = request.getRequestDispatcher("detailstock/edit.jsp");
        }
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createDetailStock(HttpServletRequest request, HttpServletResponse response) {
        int idPhone = Integer.parseInt("ID Phone");
        int phoneAmount = Integer.parseInt("Phone Amount");
        String deleteBy = request.getParameter("Delete By");
        String deleteDate = request.getParameter("Delete Date");
        String modifyBy = request.getParameter("Modify By");
        String modifyDate = request.getParameter("Modify Date");
        String createBy = request.getParameter("Create By");
        String createDate = request.getParameter("Create Date");
        int idStock = (int)(Math.random() * 10000);

        DetailStock detailstock = new DetailStock(idStock, idPhone, phoneAmount,
                deleteBy, deleteDate, modifyBy, modifyDate, createBy, createDate);
        this.detailstockService.save(detailstock);
        RequestDispatcher dispatcher = request.getRequestDispatcher("detailstock/create.jsp");
        request.setAttribute("message", "New detail stock was created");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher = request.getRequestDispatcher("detailstock/create.jsp");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listDetailStocks(HttpServletRequest request, HttpServletResponse response) {
        List<DetailStock> detailstocks = this.detailstockService.findAll();
        request.setAttribute("detailstocks", detailstocks);

        RequestDispatcher dispatcher = request.getRequestDispatcher("detailstock/list.jsp");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
