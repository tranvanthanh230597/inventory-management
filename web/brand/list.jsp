<%--
  Created by IntelliJ IDEA.
  User: tranv
  Date: 11/11/2019
  Time: 3:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Brands List</title>
</head>
<body>
<h1>Brands List</h1>
<p>
    <a href="/brands?action=create">Create new brands</a>
</p>
<table border="1">
    <tr>
        <td>Name</td>
        <td>Create By</td>
        <td>Create Date</td>
        <td>Modify By</td>
        <td>Modify Date</td>
        <td>Delete By</td>
        <td>Delete Date</td>
        <td>Edit</td>
        <td>Delete</td>
    </tr>
    <c:forEach items='${requestScope["brandList"]}' var="brand">
        <tr>
            <td><a href="/brands?action=view&id=${brand.getIdBrand()}">${brand.getName()}</a></td>
            <td>${brand.getCreateBy()}</td>
            <td>${brand.getCreateDate()}</td>
            <td>${brand.getModifyBy()}</td>
            <td>${brand.getModifyDate()}</td>
            <td>${brand.getDeleteBy()}</td>
            <td>${brand.getDeleteDate()}</td>
            <td><a href="/brands?action=edit&id=${brand.getIdBrand()}">edit</a></td>
            <td><a href="/brands?action=delete&id=${brand.getIdBrand()}">delete</a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
