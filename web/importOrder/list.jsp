<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ImportOrder List</title>
</head>
<body>
<h1>ImportOrders</h1>
<p>
    <a href="/importOrders?action=create">Create new importOrder</a>
</p>
<table border="1">
    <tr>
        <td>Name</td>
        <td>NameStaff</td>
        <td>NameStock</td>
        <td>deleteBy</td>
        <td>deleteDate</td>
        <td>modifyBy</td>
        <td>modifyDate</td>
        <td>createBy</td>
        <td>createDate</td>
        <td>Edit</td>
        <td>Delete</td>
    </tr>
    <c:forEach items='${requestScope["importOrders"]}' var="importOrder">
        <tr>
            <td><a href="/detalimportOrder?id=${importOrder.getIdImportOrder()}">${importOrder.getName()}</a></td>
            <td>${importOrder.getDeleteBy()}</td>
            <td>${importOrder.getDeleteDate()}</td>
            <td>${importOrder.getModifyBy()}</td>
            <td>${importOrder.getModifyDate()}</td>
            <td>${importOrder.getCreateBy()}</td>
            <td>${importOrder.getCreateDate()}</td>
            <td>${importOrder.getNameStaff()}</td>
            <td>${importOrder.getNameStock()}</td>
            <td><a href="/importOrders?action=edit&id=${importOrder.getIdImportOrder()}">edit</a></td>
            <td><a href="/importOrders?action=delete&id=${importOrder.getIdImportOrder()}">delete</a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>