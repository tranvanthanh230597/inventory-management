<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>DetailImportOrder List</title>
</head>
<body>
<h1>DetailImportOrders</h1>
<p>
    <a href="/detailImportOrders?action=create">Create new detailimportOrder</a>
</p>
<table border="1">
    <tr>
        <td>phoneAmount</td>
        <td>deleteBy</td>
        <td>deleteDate</td>
        <td>modifyBy</td>
        <td>modifyDate</td>
        <td>createBy</td>
        <td>createDate</td>
        <td>Edit</td>
        <td>Delete</td>
    </tr>
    <c:forEach items='${requestScope["detailImportOrders"]}' var="detailImportOrder">
        <tr>
            <td><a href="/importOrders?action=view&id=${importOrder.getIdImportOrder()}">${importOrder.getPhoneAmount()}</a></td>
            <td>${importOrder.getDeleteBy()}</td>
            <td>${importOrder.getDeleteDate()}</td>
            <td>${importOrder.getModifyBy()}</td>
            <td>${importOrder.getModifyDate()}</td>
            <td>${importOrder.getCreateBy()}</td>
            <td>${importOrder.getCreateDate()}</td>
            <td><a href="/importOrders?action=edit&id=${importOrder.getIdImportOrder()}">edit</a></td>
            <td><a href="/importOrders?action=delete&id=${importOrder.getIdImportOrder()}">delete</a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>